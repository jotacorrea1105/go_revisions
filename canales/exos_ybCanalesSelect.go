package main

import (
	"fmt"
	//"sync"
	//"runtime"
)

func main(){

	/*
	//Exo1
	*/
	par := make(chan int)
	impar := make(chan int)
	salir := make(chan int) 
	
	//enviar
	go enviar(par, impar, salir)
	//recibir
	recibir(par, impar, salir)

	fmt.Println("-------Finalizando-------")


}

func enviar(p chan<- int, i chan<- int, s chan<- int){
	
	for j:=0; j<100; j++{
		if(j%2 == 0){
			p <- j
		}else{
			i <- j
		}
		 
	}
	
	s <- 0
}

func recibir(p, i, s <-chan int){

	for{
		select{
		case v:= <- p:
			fmt.Println("-desde el canal par: \t", v)
		case v:= <- i:
			fmt.Println("-desde el canal impar: \t", v)
		case v:= <- s:
			fmt.Println("-desde el canal salir: \t", v)
			return
		}
	}
}