package main

import (
	"fmt"
	//"sync"
	//"runtime"
)

func main(){

	/*
	//Exo1
	*/
	ca := make(chan int, 2)
		
	go enviar(ca)

	recibir(ca) // no es una gorouitina porque solo necesita extraer el valor del canal e imprimirlo

	fmt.Println("-------Finalizando-------")


}

func enviar(c chan<- int){
	c <- 42
}

func recibir(c <-chan int){
	fmt.Println(<-c)
}
