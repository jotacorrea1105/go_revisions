package main

import (
	"fmt"
	//"sync"
	//"runtime"
)

func main(){

	/*
	//Exo1
	*/
	ca := make(chan int) 
		
	go func(){
		for i :=0; i<4; i++{
			ca <- i
		}
		close(ca) //para cerrar los canales y que las funciones receptoras no sigan esperando despues del final
	}()

	for v := range ca{
		fmt.Println(v)
	}

	fmt.Println("-------Finalizando-------")


}