package main

import (
	"fmt"
)

const null int = -1
const nbNodes int = 4
const nbTours int = 4

const jeton int = -2
const idJeton int = 1

func main() {

	ce := make(chan int)
	cs := make(chan int)
	fin := make(chan bool)
	var tabCan [nbNodes]chan int
	var tabValeurs = [nbNodes]int{10, 6, 9, 4}

	for i := 0; i < nbTours; i++ {
		go func(j int) {

			node(tabValeurs[j], (j+1)%nbNodes, ce, cs, j == idJeton)
			fin <- true
		}(i)

	}
	for i := 0; i < nbNodes; i++ {
		<-fin
	}
	fmt.Println("\nFin du programme")
	fmt.Println("---------")
}

func node(entier, id int, in, out chan int, aLeJeton bool) {

	var valRecue int
	for i := 0; i < nbTours; i++ {

		go func() {
			valRecue = <-in
			fmt.Println("j'ai recu le jeton", "id: ", id, "i: ", i, "valRecue: ", valRecue)
		}()

		if aLeJeton == true {
			go func() {
				out <- jeton

			}()
		}

		//fmt.Println("id: ", id, "i: ", i, "valRecue: ", valRecue)
	}

}
