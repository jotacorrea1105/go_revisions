package main

import (
	"fmt"
)

const null int = -1
const nbNodes int = 4
const nbTours int = 4

const jeton int = -2
const idJeton int = 1

func main() {

	ce := make(chan int)
	cs := make(chan int)
	fin := make(chan bool)

	for i := 0; i < nbTours; i++ {
		go func(j int) {

			node((j+1)%nbNodes, ce, cs, j == idJeton)
			fin <- true
		}(i)

	}
	for i := 0; i < nbNodes; i++ {
		<-fin
	}
	fmt.Println("\nFin du programme")
	fmt.Println("---------")
}

func node(id int, in, out chan int, aLeJeton bool) {

	var valRecue int
	for i := 0; i < nbTours; i++ {

		go func() {
			valRecue = <-in

		}()

		if aLeJeton == true {
			go func(i int) {
				out <- jeton
				fmt.Println("j'ai recu le jeton", "id: ", id, "i: ", i, "valRecue: ", valRecue)
			}(i)
		}

		//fmt.Println("id: ", id, "i: ", i, "valRecue: ", valRecue)
	}

}
