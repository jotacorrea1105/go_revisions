package main

import (
	"fmt"
)

const null int = -1
const nbNodes int = 4
const nbTours int = 4

func main() {

	ce := make(chan int)
	cs := make(chan int)
	fin := make(chan bool)

	for i := 0; i < nbTours; i++ {
		go func(j int) {

			node((j+1)%nbNodes, ce, cs)
			fin <- true
		}(i)

	}
	for i := 0; i < nbNodes; i++ {
		<-fin
	}
	fmt.Println("\nFin du programme")
	fmt.Println("---------")
}

func node(id int, in, out chan int) {

	var valRecue, valEnvoyee int

	for i := 0; i < nbTours; i++ {
		go func() {
			valRecue = <-in

		}()
		go func() {
			out <- valEnvoyee

		}()
		fmt.Println("id: ", id, "i: ", i, "valRecue: ", valRecue)
	}

}
