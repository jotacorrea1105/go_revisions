package main

import (
	"fmt"
	"time"
)

const null int = -1
const nbNodes int = 4
const nbTours int = 4

func main() {

	ce := make(chan int)
	cs := make(chan int)
	fin := make(chan bool)

	now := time.Now()

	for i := 0; i < nbTours; i++ {
		go func(j int) {

			node((j+1)%nbNodes, ce, cs, now)
			fin <- true
		}(i)

	}
	for i := 0; i < nbNodes; i++ {
		<-fin
	}
	fmt.Println("\nFin du programme")
	fmt.Println("---------")
}

func node(id int, in, out chan int, now time.Time) {

	var valRecue, valEnvoyee int

	for i := 0; i < nbTours; i++ {
		go func() {
			valRecue = <-in

		}()
		go func() {
			out <- valEnvoyee

		}()
		time.Sleep(time.Duration(id*id) * time.Second)
		fmt.Println("id: ", id, "i: ", i, "valRecue: ", valRecue, "temps ", time.Since(now))
	}

}
