package main

import (
	"fmt"
)

const nbX int = 10

func main() {
	//c1 := make(chan int) //canal de entrada
	//c2 := make(chan int) //canal de salida
	s := make(chan bool) //confirmation

	const nbProc int = 3

	var tabCan [nbProc + 1]chan int
	for i := range tabCan {
		tabCan[i] = make(chan int) //i:0 = source, i:1 = traitement, i:2 = affichage
	}

	tabK := [3]int{2, 3, 4}

	for i := 0; i < nbProc; i++ {
		go func(j int) {
			traitement(tabCan[j], tabCan[j+1], tabK[j])
			s <- true
		}(i)
	}
	go func() {
		affichage(tabCan[nbProc])
		s <- true
	}()

	go func() {
		source(tabCan[0])
		s <- true
	}()

	<-s
	<-s
	<-s

}

func source(out chan int) {
	for i := 0; i < nbX; i++ {
		out <- i + 1
	}
}

func traitement(in chan int, out chan int, k int) {
	var valIn, valOut int
	for i := 0; i < nbX; i++ {
		valIn = <-in
		valOut = valIn + k
		out <- valOut
	}
}

func affichage(in chan int) {
	var resIn int
	for i := 0; i < nbX; i++ {
		resIn = <-in
		fmt.Println(resIn)
	}
}
