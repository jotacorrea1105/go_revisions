package main

import (
	"fmt"
)

func main() {

	x := make(chan int)
	go func() {
		fmt.Println("Hello")
		//x <- 1
	}()
	<-x
	fmt.Println("world")
}
