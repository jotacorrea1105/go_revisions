package main

import (
	"fmt"
)

const valConst int = 10

func main() {

	ce := make(chan int)
	cs := make(chan int)
	confirmation := make(chan bool)
	go func() {
		source(cs)
		confirmation <- true
	}()

	go func() {
		go seuillage(20, ce, cs)
		confirmation <- true
	}()

	go func() {
		go affichage(cs)
		confirmation <- true
	}()

	<-confirmation
	<-confirmation
	<-confirmation

}
func source(valDefaut chan int) {
	image := [9]int{100, 200, 150, 32, 250, 18, 47, 242, 99}

	for i := 0; i < 9; i++ {
		valDefaut <- image[i]
	}

}
func seuillage(valSeuil int, valIn chan int, valOut chan int) {
	var valuerRentrant int

	for i := 0; i < 9; i++ {
		valuerRentrant = <-valIn
		if valuerRentrant < valSeuil {
			valOut <- 0
		} else {
			valOut <- 1
		}
	}

}

func affichage(valAffiche chan int) {
	var in int
	in = <-valAffiche
	for i := 0; i < 9; i++ {
		fmt.Println(in)
	}

}
