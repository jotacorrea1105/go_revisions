package main

import (
	"fmt"
)

const valConst int = 10

func main() {

	ce := make(chan int)
	cs := make(chan int)
	confirmation := make(chan bool)
	go func() {
		source(cs)
		confirmation <- true
	}()

	go func() {
		go seuillage(20, ce, cs)
		confirmation <- true
	}()

	go func() {
		go affichage(cs)
		confirmation <- true
	}()

	<-confirmation
	<-confirmation
	<-confirmation

}
func source(valDefaut chan int) {

	valDefaut <- valConst
}
func seuillage(valSeuil int, valIn chan int, valOut chan int) {
	var valuerRentrant int
	valuerRentrant = <-valIn

	if valuerRentrant < valSeuil {
		valOut <- 0
	} else {
		valOut <- 1
	}
}

func affichage(valAffiche chan int) {
	var in int
	in = <-valAffiche
	fmt.Println(in)
}
