package main

import (
	"fmt"
)


func main(){

	/*
	//EXO1P4
	
	x:=[]int {1,2,3,4,5}

	v := [5] int {1,2,3,4,5}
	for i := range x {
		fmt.Println(v[i])
		fmt.Printf(" valor de x[i] %v tipo de valores de x: %T \n", x[i], x[i])
	}
	*/

	/*
	//EXO2P4
	x:=[]int {42,43,44,45,46,47,48,49,50,51}
	for i, v := range x {
		fmt.Println(i,v)
	}
	fmt.Printf("%T\n", x)
	*/

	/*
	//EXO3P4
	
	x:=[]int {42,43,44,45,46,47,48,49,50,51}
	
	a := append(x[:5])
	fmt.Println(a)
	
	b := append(x[5:])
	fmt.Println(b)
	
	c := append(x[2:7])
	fmt.Println(c)
	
	d := append(x[1:6])
	fmt.Println(d)
	*/

	/*
	//EXO4P4
	
	x:=[]int {42,43,44,45,46,47,48,49,50,51}
	
	x = append(x, 52)
	fmt.Println(x)
	
	x = append(x, 53, 54, 55)
	fmt.Println(x)
	
	b := []int {56, 57, 58, 59, 60}
	
	x = append(x, b...)
	fmt.Println(x)
	*/

	/*
	//EXO5P4
	
	x:=[]int {42,43,44,45,46,47,48,49,50,51}
		
	b := append(x[:3], x[6:]...)
	fmt.Println(b)
	*/
	/*
	//EXO6P4
	
	x:= make([]string, 5, 5)
	x = []string {"Pedro", "Pablo", "Paco", "Arsecio", "Veneco"}
	
	for i:=0; i<len(x); i++{
		fmt.Println(i, x[i])
	}
	fmt.Println(cap(x))
	*/
	/*
	//EXO7P4
	
	x:= []string {"Batman", "Jefe", "Vestido de negro"}
	y := []string {"Robin", "Ayudante", "Vestido de colores"}
	sm := [][]string {x, y}	

	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(sm)
	for i, reg :=range sm{
		fmt.Println("registro: ",i)

		for j, val:=range reg{
			fmt.Printf("\tindice: %v\tvalor:%v\n", j, val)
		}
	}
	*/
	
	/*
	//EXO8P4
	
	x:= map[string][]string{
		"eduar_tua":[]string{"computadoras", "montanha", "playa"},
		"carlos_ramon":[]string{"leer","comprar", "musica"},
		"juan_bimba":[]string{"helado", "pintar", "bailar"},
		
	}

	fmt.Println(x)

	for k, v:=range(x){
		fmt.Printf("llaves: %v\n", k)
		for i, valor:=range(v){
			fmt.Printf("indice: %v\t valor: %v\n", i, valor)
		}
	}
	*/
	/*
	//EXO9P4
	
	x:= map[string][]string{
		"eduar_tua":[]string{"computadoras", "montanha", "playa"},
		"carlos_ramon":[]string{"leer","comprar", "musica"},
		"juan_bimba":[]string{"helado", "pintar", "bailar"},
		
	}

	fmt.Println(x)

	x ["luis_salcedo"]=[]string{"cicla","perros","gatos"}

	for k, v:=range(x){
		fmt.Printf("llaves: %v\n", k)
		for i, valor:=range(v){
			fmt.Printf("indice: %v\t valor: %v\n", i, valor)
		}
	}
	*/
	/*
	//EXO10P4
	*/
	x:= map[string][]string{
		"eduar_tua":[]string{"computadoras", "montanha", "playa"},
		"carlos_ramon":[]string{"leer","comprar", "musica"},
		"juan_bimba":[]string{"helado", "pintar", "bailar"},
		
	}

	x ["luis_salcedo"]=[]string{"cicla","perros","gatos"}
	fmt.Println(x)

	delete(x, "luis_salcedo")
	fmt.Println(x)
	
	for k, v:=range(x){
		fmt.Printf("llaves: %v\n", k)
		for i, valor:=range(v){
			fmt.Printf("indice: %v\t valor: %v\n", i, valor)
		}
	}

	v, ok:= x["luis_salcedo"]
	fmt.Println(v, ok)
}
