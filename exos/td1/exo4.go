package main

import (
	"fmt"
)

func main() {

	s1 := make(chan bool)
	var tabCanCtrl, tabElus [3]chan int

	for i := 0; i < 3; i++ {
		tabCanCtrl[i] = make(chan int)
		tabElus[i] = make(chan int)
	}

	for i := 0; i < 3; i++ {
		go user(i, tabCanCtrl[i], tabElus[i])
	}

	go controle(tabCanCtrl, tabElus)

	<-s1
	fmt.Println(" f i n du main ")
}

func user(id int, idTab, elus chan int) {
	var elu int

	for {
		idTab <- id

		elu = <-elus

		if elu == id {
			fmt.Println("user ", id, "moi l'elu")
		} else {
			fmt.Println("user ", id, "moi le pas elu")
		}
	}
}

func controle(idTabCtrl, eluTab [3]chan int) {
	var valeur int
	var idmax int

	for {
		idmax = -1
		for i := range idTabCtrl {
			valeur = <-idTabCtrl[i]
			if valeur > idmax {
				idmax = valeur
			}
		}
		for i := range eluTab {
			eluTab[i] <- idmax
		}
	}
}
