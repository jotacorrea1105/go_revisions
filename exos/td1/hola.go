package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {

	//fmt.Println(runtime.NumGoroutine())
	//fmt.Println(runtime.NumCPU())
	s1 := make(chan bool)
	s2 := make(chan bool)
	var c chan int
	c = make(chan int)

	//wg.Add(2)
	go func() {
		A(c)
		s1 <- true
	}()
	go func() {
		B(c)
		s2 <- true
	}()

	<-s1
	<-s2
	//wg.Wait()

	fmt.Println(" f i n du main ")
}
func A(out chan int) {
	out <- 3
	//wg.Done()
}
func B(in chan int) {
	/*var  x int
	x = <- in
	*/
	fmt.Println(<-in)
	//wg.Done()
}
