package main

import (
	"fmt"
)

func main() {

	s1 := make(chan bool)
	s2 := make(chan bool)
	s3 := make(chan bool)
	s4 := make(chan bool)

	go func() {
		A()
		s1 <- true
	}()
	go func() {
		B()
		s2 <- true
	}()
	<-s1
	<-s2
	go func() {

		C()
		s3 <- true
	}()

	go func() {

		D()
		s4 <- true
	}()
	<-s3
	<-s4

	fmt.Println(" f i n du main ")
}
func A() {
	fmt.Println("A")
}
func B() {
	fmt.Println("B")
}
func C() {
	fmt.Println("C")
}
func D() {
	fmt.Println("D")
}
