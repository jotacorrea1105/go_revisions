package main

import "fmt"

//"sync"
//"runtime"

func main() {

	/*
		//Exos de estudios
	*/

	/*
		//EXO1P10

		//1.1
		ca := make(chan int)
		go func() {
			ca <- 42
		}()

		fmt.Println(<-ca)


		//1.2
		ca := make(chan int, 1)
		ca <- 42
		fmt.Println(<-ca)
	*/
	/*
		//EXO2P10

		//1
		cs := make(chan int)
		go func() {
			cs <- 42
		}()

		fmt.Println(<-cs)
		fmt.Printf("------------\n")
		fmt.Printf("cs\t %T\n", cs)


		//2
		cr := make(chan int)
		go func() {
		cr <- 42
		}()

		fmt.Println(<-cr)
		fmt.Printf("------------\n")
		fmt.Printf("cs\t %T\n", cr)
	*/
	/*
		//EXO3P10

		//1
		c := gen()
		recibir(c)
		fmt.Printf("------finalizando------\n")

		func recibir(c <-chan int) {
			for i := range c {
				fmt.Println(i)
				fmt.Printf("------------\n")
				fmt.Printf("c est\t %T\n", c)
			}

		}

		func gen() <-chan int {
			c := make(chan int)
			go func() {
				for i := 0; i < 10; i++ {
					c <- i
				}
				close(c)
			}()
			return c
		}
	*/
	/*
		//EXO6P10
		//1

		c := make(chan int)

		go func() {
			for i := 0; i < 100; i++ {
				c <- i
			}
			close(c)

		}()

		for v := range c {
			fmt.Println(v)
		}
		/**/
	//EXO7P10
	//1
	c := make(chan int)
	for j := 0; j < 10; j++ {
		go func() {
			for i := 0; i < 10; i++ {
				c <- i
			}
		}()
	}
	for v := 0; v < 100; v++ {
		fmt.Println(v, <-c)
	}

}
