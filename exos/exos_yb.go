package main

import "fmt"

/*EXO2/3
	var x int = 42
	var y string = "James Bond"
	var z bool = true
*/

//EXO4
type numero int
var x numero
var y int

func main(){
	/*
	//EXO1
	x:= 42
	y:= "James Bond"
	z:= true

	//fmt.Println(x, y, z)
	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(z)
	*/

	/*
	//EXO2/3
	s := fmt.Sprintf("%v\t%v\t%v", x, y, z)
	fmt.Println(s)
	
	fmt.Println(y)
	fmt.Println(z)
	*/

	//EXO4/5
	
	fmt.Println(x)
	fmt.Printf("type de x: %T\n", x)

	x = 42
	fmt.Println(x)
	
	y=int(x)
	fmt.Println(y)
	fmt.Printf("type de y: %T\n", y)

}
