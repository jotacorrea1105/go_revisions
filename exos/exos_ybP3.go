package main

import (
	"fmt"
)


func main(){

	/*
	//EXO3P3
	
	for i:=1;i<=22;i++{

		fmt.Println("Anhos", i+2000)
	}
	*/
	/*
	//EXO4P3
	
	i := 2001

	for {
		
		fmt.Println("Anhos", i)
		if i == 2023 {
			break;
		}
		i++
	}
	*/
	/*
	//EXO5P3
	
	for i:= 10;i<100; i++ {
		
		fmt.Printf("modulo de %v es: %v\n ", i, i%4)
			
		
	}
	*/
	/*
	//EXO6P3
	
	x := "hola"
	if x == "hola"{
		fmt.Println("verdadero")
	}
	*/
	/*
	//EXO6P3
	
	x := "hola"
	if x == "holaa"{
		fmt.Println("falso")
	}else if x == "que tal"{
		fmt.Println("false")
	}else{
		fmt.Println("verdadero")
	}
	*/

	/*
	//EXO9P3 --> switch
	*/

	/*
	//EXO10P3 

	fmt.Println(true&&true)
	fmt.Println(true&&false)
	fmt.Println(true||true)
	fmt.Println(true||false)
	fmt.Println(!true)
	*/


}
