package main

import "fmt"

func main() {
	
	in := make (chan int);
	out := make (chan int);
	

	go source(in, out)

	go seuillage(2, in, out)

	go affichage(in, out)
}

func source(in chan int, out chan int){
	var entier int = 1
	out <- entier
}

func seuillage( valeurSeuil int, in chan int, out chan int){
	var Pixelrecu int = <- out
	in <- Pixelrecu 

	if Pixelrecu < valeurSeuil {
		out <- 0
	} else {
        out <- 1
	}
}
func affichage(in chan int, out chan int){
	var valeurRecu int = <-in 
	fmt.Println(valeurRecu);
}