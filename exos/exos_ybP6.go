package main

import (
	"fmt"
)


func main(){

	/*
	//EXO1P6
	
	
	a := foo()
	fmt.Println(a)

	b, c := bar()
	fmt.Println(b, c)

	*/

	/*
	//EXO2P6
	
	a := foo(1, 2, 3, 4, 5)
	fmt.Println(a)

	tab := []int {1, 2, 3, 4, 5}
	b := bar (tab)
	fmt.Println(b)
	*/

	/*
	//EXO3P6
	
	defer foo(1, 2, 3)

	tab := []int {1, 2, 3, 4, 5}
	fmt.Println(tab)
	*/

	/*
	//EXO4P6
	*/

}

func foo(x... int) int{
	sum := 0

	for i:=range x{
		sum = sum + x[i]
	}
	fmt.Println(sum)
	return sum 
}
