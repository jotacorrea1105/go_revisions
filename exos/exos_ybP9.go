package main

import (
	"fmt"
	"sync"
	//"runtime"
)

func main(){

	/*
	//EXO1P9
	
	var wg sync.WaitGroup

	fmt.Println("Go rutina princ")
	wg.Add(2)

	go func() {
		fmt.Println("Go rutina sec 1")
		wg.Done()
		
	}()
	go func() {
		fmt.Println("Go rutina sec 2")
		wg.Done()
		
	}()
	wg.Wait()
	*/

	/*
	//EXO3P9
	
	var wg sync.WaitGroup
	const gs = 100
	
	cpt := 0

	fmt.Println("Go rutina princ")
	wg.Add(gs)

	for i:=0;i<gs; i++{
		go func() {
			
			
			newCpt := cpt
			runtime.Gosched()

			newCpt ++
			fmt.Println("Go rutina N°: ", cpt)

			cpt = newCpt
			wg.Done()
			
		}()
		fmt.Println("N° de goroutines: \t", i)
	}

	wg.Wait()
	*/

	/*
	//EXO4P9
	
	var wg sync.WaitGroup
	const gs = 100
	
	cpt := 0

	fmt.Println("Go rutina princ")
	wg.Add(gs)

	var mu sync.Mutex

	for i:=0;i<gs; i++{
		go func() {
			
			mu.Lock()	

			newCpt := cpt
			newCpt ++
			cpt = newCpt
			
			mu.Unlock()
			
			fmt.Println("incremento N°: ", cpt)
			wg.Done()
			
		}()
		fmt.Println("N° de goroutines: \t", i)
	}

	wg.Wait()
	*/

	/*
	//EXO7P9
	*/
	
	var wg sync.WaitGroup
	const gs = 100
	
	cpt := 0

	fmt.Println("Go rutina princ")
	wg.Add(gs)

	var mu sync.Mutex

	for i:=0;i<gs; i++{
		go func() {
			
			mu.Lock()	

			newCpt := cpt
			newCpt ++
			cpt = newCpt
			
			mu.Unlock()
			
			fmt.Println("incremento N°: ", cpt)
			wg.Done()
			
		}()
		fmt.Println("N° de goroutines: \t", i)
	}

	wg.Wait()
	
}
